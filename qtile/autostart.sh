#!/usr/bin/env bash
#

gentoo-pipewire-launcher &

pgrep dunst || dunst --startup_notification &
pgrep volumeicon || volumeicon &


feh --bg-scale --randomize `xdg-user-dir PICTURES`/Wallpapers &

pgrep mpd || mpd &
